# Flashcard Application

![Image](/media/defaultp.jpg)
## Description

This is a modern way to memorize information. The user has the ability to create or edit their own set of flashcards for maximum study efficiency. Answers are displayed on cards at the user's request, allowing for a flexible and customizable learning experience. The application utilizes a variety of web technologies and APIs to provide a rich, interactive user experience.

## Table of Contents of this README
- [Description](#description)
- [Project Structure](#project-structure)
- [Functionality](#functionalities)
- [Requirements](#requirements)
  - [HTML](#html)
  - [CSS](#css)
  - [JavaScript](#javascript)
- [Development and Launch](#development-and-launch)
- [For Any Questions](#for-any-questions)

## Project Structure

- `css/`: Contains all the CSS files for the project.
- `js/`: Contains all the JavaScript files for the project.
- `media/`: Contains all the image, video, and audio files used in the project.
- `pages/`: Contains all pages that are used in the project.
- `index.html`: The main HTML file at the root level.
- `README.md`: This file, containing a detailed description of the usage and purpose of the Flashcard App.

## Functionalities
Page's main purpose is providing simple and intuitive environment for *creaing*, *deleting*, *updating* flashcards.
Example of usage:
1. Click "Add flashcard" to create a new card on the screen.
2. Fill out the fields and choose an image (not mandatory).
3. Added flashcards can be edited or deleted.
4. To view or hide the answer, press "Show/Hide".
5. On help page is possible to guess location by IP address (experimental functionality).
6. Enjoy the experience.

## Requirements

A quick description of the fulfilled requirements for easier localization.
### Documentation
- **Documentation**: current `README.md` file.
### HTML
- **Validity**: All pages have passed the W3C validator (checked through Direct input).
- **Cross-Browser Compatibility**: The page functions in all popular browsers (Chrome, Opera, Firefox, Edge).
- **Semantic Tags**: Page structure includes and correctly uses header, section, and footer.
- **Graphics - SVG/Canvas**: The "Add flashcard" button on the main screen.
- **Media - Audio/Video**: Title in navigation, video, audio on card delete event.
- **Form Elements**: Add flashcard form on the main screen (autofocus, switch between fields with KeyRight, KeyLeft), types: text, image.
- **Offline Application**: Offline use is supported.

### CSS
- **Advanced Selectors**: Use of Pseudo-Class Selector (:hover), Descendant Selector (space), :before selector for animations.
- **Vendor Prefixes**: Found in `css/help.css` and `css/styles.css`.
- **CSS3 Transitions/Animations**: Title in navigation.(Works on load or on refresh, only on home page).
- **CSS3 2D/3D Transformations**: Flipping card animation on the Help page.
- **Media Queries**: Found in `css/intro.css`, `css/help.css`, `css/styles.css` for cases of max-width 480px and 900px and so on. 

### JavaScript
- **OOP Approach**: Followed in `js/` folder files.
- **Use of Advanced JS APIs**: LocalStorage for saving cards, Drag&DropAPI and FileAPI for uploading pictures.
- **Functional History**: History API is used in `js/router.js`.
- **Media Control**: Sound on delete of flashcard.
- **Offline Application**: Internet connection state checked from `js/intro.js` with corresponding reactions.
- **JS SVG Work**: Intro animation.

## Development and Launch

The project uses:
- JavaScript, HTML, and CSS.
- No external dependencies.

Launch:
1. Choose browser. (Most frequently tested in Chrome)
2. **Important**: Ensure you are located at the root level of the project. Open `index.html`.
3. Use LiveServer, preferably on port 5500.
4. Done.

### For Any Questions
or if the grade is less than 19 points.

Please contact email:
 ilienvik@cvut.cz

Thank you for taking the time to review the Flashcard project. I hope it works well for you :)
