function SPARouter(routes) {
    this.routes = routes;
    this.currentScript = null;
    this.isFirstRun=true;
    this.loadedScripts = {};
    this.init();
}

SPARouter.prototype.init = function() {
    //handling online and offline events
    function updateOfflineMessage() {
        const offlineMessage = document.getElementById('offline-message');
        if (!navigator.onLine) { 
            offlineMessage.style.display = 'block';
        } else { 
            offlineMessage.style.display = 'none'; 
        }
    }

    window.addEventListener('online', updateOfflineMessage);
    window.addEventListener('offline', updateOfflineMessage);
    updateOfflineMessage();
  
    window.onpopstate = () => this.handleLocation();
    document.addEventListener("DOMContentLoaded", () => this.handleLocation());
    this.attachEventListeners();
};

SPARouter.prototype.attachEventListeners = function() {
    document.querySelectorAll('a[data-route]').forEach(anchor => {
        anchor.addEventListener('click', (event) => this.route(event));
    });
};
//catching nav changes, routing to correct page
SPARouter.prototype.route = async function(event) {
    event.preventDefault();
    const targetId = event.target.getAttribute("data-id"); 
    if (window.location.hash==="#help"&&  `${targetId}`==="home") {
        localStorage.setItem('introPlayed',true); 
        this.isFirstRun=false;
    }
    if (window.location.hash !== `#${targetId}`) {
        window.location.hash = targetId;
        await this.handleLocation();
    }
};

SPARouter.prototype.handleLocation = async function() {
    let hash = window.location.hash.replace('#', '');
  
    //making sure intro plays only once on start and on refresh of home page
    if(this.isFirstRun){ 
            localStorage.removeItem('introPlayed'); 
            this.isFirstRun=false;
    };
         
    
    if (hash === '') {
        hash = 'home'; // Redirect to home page if hash is empty
    }

    if (!this.routes[hash]) {
        hash = '404';  //only catches certain types of errors:(
    }

    const route = this.routes[hash] || this.routes['404'];
    const html = await fetch(route.path).then((data) => data.text());
    document.getElementById("main-page").innerHTML = html;
    document.title = route.title;
    this.loadScriptIfNeeded(hash);
};
// adding page specific js
SPARouter.prototype.loadScriptIfNeeded = function(hash) {
    const scriptMap = {
        "home": "js/intro.js",
        "help": "js/help.js" 
    };

    const scriptSrc = scriptMap[hash];

    if (scriptSrc) {
        if (!this.loadedScripts[scriptSrc]) {
            if (this.currentScript) {
                document.body.removeChild(this.currentScript);
                this.currentScript = null;
            }

            this.currentScript = document.createElement('script');
            this.currentScript.src = scriptSrc;
            this.currentScript.onload = () => {
                this.loadedScripts[scriptSrc] = true;
                this.initializePage(hash);
            }; 
            document.body.appendChild(this.currentScript);
        } else {
            this.initializePage(hash);
        }
    }
};

SPARouter.prototype.initializePage = function(hash) {
    if (hash === "home") { 
        new GSAPAnimation();
    } else if (hash === "help") {
        new HelpPage();
    }
};

const routes = {
    404: { path: "/pages/404.html", title: "404 - Page Not Found" },
    "home": { path: "/pages/index.html", title: "Home - FlashCard Application" },
    "help": { path: "/pages/help.html", title: "Help - FlashCard Application" },
};

const router = new SPARouter(routes);
window.route = (event) => router.route(event);
