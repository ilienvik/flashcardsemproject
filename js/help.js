function HelpPage() {
    this.locationPara = document.getElementById("location");
    this.btn = document.getElementById("geoLoc");
    this.initialize();
    this.hideIntroContainer();  
} 

HelpPage.prototype.initialize = function() {
    this.btn.addEventListener("click", () => this.fetchLocation());
    this.autoHideTitle();
};
HelpPage.prototype.autoHideTitle = function() {
    setTimeout(() => {
        const video = document.getElementById("video");
        const title = document.getElementById("title");

        if (video) {
            video.style.display = "none";
        }

        if (title) {
            title.style.animation = "none";
            title.classList.add("no-animation");
            const spans = title.querySelectorAll(".animation");
            spans.forEach(span => span.style.color = "white");
        }
    }, 10000);
};
//geolocation guess
HelpPage.prototype.fetchLocation = function() {
    fetch("https://ipapi.co/json/")
        .then(response => response.json())
        .then(data => this.displayLocation(data))
        .catch(error => this.handleError(error));
};

HelpPage.prototype.displayLocation = function(data) {
    const city = data.city;
    const country = data.country_name;
    this.locationPara.textContent = `Location: ${city}, ${country}`;
    this.btn.innerHTML = "Your location";
};

HelpPage.prototype.handleError = function(error) {
    console.error("Error fetching location:", error);
    this.locationPara.textContent = "Error fetching location.";
};

HelpPage.prototype.hideIntroContainer = function() {
   
    const video = document.getElementById("video");
    video.style.display = "none";
    const title = document.querySelector("h1");
    const titleBefore = window.getComputedStyle(title, '::before');
    
    //stopping the animation
    function stopBeforeAnimation() {
        
        const style = document.createElement('style');
        style.innerHTML = `
            header{
                padding: 2vh 2vw;
    height: 8vh;
            }
            h1::before {
                width:0vw;
                animation: none !important;
            }
        `;
        
        document.head.appendChild(style);
    }
    stopBeforeAnimation();
    const introContainer = document.querySelector('.intro-container');
    if (introContainer) {
        
        introContainer.style.display = 'none';
    }
};
