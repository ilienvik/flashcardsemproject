function HomePage() {
    this.flashcards = [];
    this.isBeingEdited = false;
    this.editedCardIndex = -1;
    this.init();
}

HomePage.prototype.init = function() {
    this.setupElements(); 
    this.attachEventListeners();
    this.loadFlashcards();
    this.autoHideTitle();
    this.getJoke();
};

HomePage.prototype.setupElements = function() {
    this.mainContainer = document.querySelector(".main-container");
    this.addQuestionCard = document.getElementById("add-question-card");
    this.cardButton = document.getElementById("save-btn");
    this.question = document.getElementById("question");
    this.answer = document.getElementById("answer");
    this.imageInput = document.getElementById("image");
    this.dropArea = document.getElementById("drop-area");
    this.preview = document.getElementById("preview");
    this.errorMessage = document.getElementById("error-message");
    this.addQuestion = document.getElementById("add-flashcard");
    this.closeBtn = document.getElementById("close-btn");
    this.cardList = document.querySelector(".card-list");
    this.jokeContainer = document.getElementById("joke");
    this.btn = document.getElementById("bttn");
};

HomePage.prototype.autoHideTitle = function(){
    //hiding animation after first play and making title reappear
    setTimeout(() => {
        const video = document.getElementById("video");
        const title = document.getElementById("title");

        if (video) {
            video.style.display = "none";
        }

        if (title) {
            title.style.animation = "none";
            title.classList.add("no-animation");
            const spans = title.querySelectorAll(".animation");
            spans.forEach(span => span.style.color = "white");
        }
    }, 10000);
};
//load joke if user is online
HomePage.prototype.getJoke = async function() {
    if (navigator.onLine) { const url = "https://v2.jokeapi.dev/joke/Any?blacklistFlags=nsfw,religious,political,racist,sexist,explicit&type=single";
    try {
        const response = await fetch(url);
        const data = await response.json();
        if (data.joke.length > 120) {
            this.getJoke();
        } else {
            this.jokeContainer.textContent = `${data.joke}`;
            this.jokeContainer.classList.add("fade");
        }
    } catch (error) {
        console.error("Error fetching joke:", error);
    }}
};
//move through text fields with keyboard
HomePage.prototype.attachEventListeners = function() {
    this.question.addEventListener("keydown", (event) => {
        if (event.key === "ArrowRight" || event.key === "ArrowDown") {
            event.preventDefault();
            this.answer.focus();
        }
    });

    this.answer.addEventListener("keydown", (event) => {
        if (event.key === "ArrowLeft" || event.key === "ArrowUp") {
            event.preventDefault();
            this.question.focus();
        }
    });

    this.btn.addEventListener("click", () => this.getJoke());

    this.addQuestion.addEventListener("click", () => {
        this.mainContainer.classList.add("hide");
        this.question.value = "";
        this.answer.value = "";
          this.preview.src = "";
        this.preview.classList.add('hide');
        this.addQuestionCard.classList.remove("hide");
        this.question.focus();
        this.question.setAttribute("required", true);
        this.answer.setAttribute("required", true);
    });

    this.closeBtn.addEventListener("click", () => this.hideQuestion());

    //process form submission with validation
    this.cardButton.addEventListener("click", (event) => {
        event.preventDefault(); 
        const sanitizedQuestion = this.sanitizeInput(this.question.value.trim());
        const sanitizedAnswer = this.sanitizeInput(this.answer.value.trim());
        this.submitQuestion(sanitizedQuestion, sanitizedAnswer);
    });

    //image upload/drag and drop methods
    this.dropArea.addEventListener('dragover', (event) => {
        event.preventDefault();
        event.stopPropagation();
        this.dropArea.classList.add('highlight');
    });

    this.dropArea.addEventListener('dragleave', (event) => {
        event.preventDefault();
        event.stopPropagation();
        this.dropArea.classList.remove('highlight');
    });

    this.dropArea.addEventListener('drop', (event) => {
        event.preventDefault();
        event.stopPropagation();
        this.dropArea.classList.remove('highlight');
        const files = event.dataTransfer.files;
        if (files.length) {
            this.handleImageUpload(files[0]);
        }
    });

    this.dropArea.addEventListener('click', () => {
       // this.imageInput.click();      //?
    });

    this.imageInput.addEventListener('change', (event) => {
        const file = event.target.files[0];
        if (file) {
            this.handleImageUpload(file);
        }
    });
};
HomePage.prototype.handleImageUpload = function(file) {
    const reader = new FileReader();

    reader.onload = (event) => {
        this.preview.src = event.target.result;
        this.preview.classList.remove('hide');
      //  this.imageInput.nextElementSibling.textContent = file.name;     /////////issue
    }; 
    reader.readAsDataURL(file);
    this.imageInput.value = "";
};
HomePage.prototype.hideQuestion = function() {
     
    this.isBeingEdited=false;
    this.editedCardIndex=-1;
    this.disableButtons(false);
    this.imageInput.nextElementSibling.textContent = "None chosen"; 
     
     this.mainContainer.classList.remove("hide");
     this.addQuestionCard.classList.add("hide");  


    //clearing all form fields and removing required attributes
    this.question.value = "";
    this.answer.value = "";
    this.preview.src = "/media/defaultp.jpg";
    this.preview.classList.add('hide');
    this.imageInput.value = "";
    this.question.removeAttribute("required");
    this.answer.removeAttribute("required");
    this.clearErrors();
};

HomePage.prototype.clearErrors = function() {
    this.errorMessage.classList.add("hide");
};

HomePage.prototype.submitQuestion = function(sanitizedQuestion, sanitizedAnswer) {
    if (!sanitizedQuestion || !sanitizedAnswer) {
        this.errorMessage.classList.remove("hide");
    } else {
        this.mainContainer.classList.remove("hide");
        this.errorMessage.classList.add("hide");
        // only saving image if it's not empty
        const imageData = this.preview.src !== "" ? this.preview.src : null; 
        if (this.isBeingEdited) {
            this.flashcards.splice(this.editedCardIndex, 1);
            this.editedCardIndex = -1;
            this.isBeingEdited = false;
        } 
        console.log("Saving flashcard:", sanitizedQuestion, sanitizedAnswer);

        this.saveFlashcard(sanitizedQuestion, sanitizedAnswer, imageData);

        this.renderFlashcards();
        this.hideQuestion(); 
    }
};

HomePage.prototype.sanitizeInput = function(input) {
    let sanitizedInput = input.replace(/</g, "&lt;").replace(/>/g, "&gt;");
    sanitizedInput = sanitizedInput.replace(/'/g, "&#39;").replace(/"/g, "&quot;");
    sanitizedInput = sanitizedInput.replace(/`/g, "&#96;");
    sanitizedInput = sanitizedInput.replace(/on\w+=".*?"/gi, "");
    return sanitizedInput;
};

HomePage.prototype.saveFlashcard = function(question, answer,image) {
    try {
        this.flashcards.push({ question, answer,image });
        localStorage.setItem("flashcards", JSON.stringify(this.flashcards));
        console.log("Flashcards after save:", this.flashcards);
    } catch (error) {
        console.error("Error saving flashcard:", error);
    }
};

HomePage.prototype.loadFlashcards = function() {
    try {
        this.flashcards = JSON.parse(localStorage.getItem("flashcards")) || [];
        this.renderFlashcards();
    } catch (error) {
        console.error("Error loading flashcards:", error);
    }
};

HomePage.prototype.renderFlashcards = function() {
    this.cardList.innerHTML = "";
    this.flashcards.forEach((flashcard, index) => {
        const card = this.createCardElement(flashcard, index);
        this.cardList.appendChild(card);
    });
};

HomePage.prototype.createCardElement = function(flashcard, index) {
    const cardDiv = document.createElement("div");
    cardDiv.classList.add("card");

    const questionPara = document.createElement("p");
    questionPara.classList.add("question-div");
    questionPara.textContent = flashcard.question;

    const answerPara = document.createElement("p");
    answerPara.classList.add("answer-div", "hide");
    answerPara.textContent = flashcard.answer;

    const imageElement = document.createElement("img");
    imageElement.classList.add("image-div", "hide");
    const pattern = /(?:index\.html|\/)$/;
if(pattern.test(flashcard.image)){flashcard.image="";} 
    if (flashcard.image&&flashcard.image!=""){
        imageElement.src = flashcard.image;
        imageElement.alt = "Image";
    } else {
        imageElement.src = "";
        imageElement.alt = "";
    }
    
    const showHideButton = document.createElement("a");
    showHideButton.href = "#";
    showHideButton.classList.add("show-hide-btn");
    showHideButton.textContent = "Show/Hide";
    showHideButton.addEventListener("click", (event) => {
        event.preventDefault();
        answerPara.classList.toggle("hide");
        imageElement.classList.toggle("hide");
    });

    const editButton = document.createElement("button");
    editButton.classList.add("edit");
    editButton.innerHTML = `<i class="fa-solid fa-pen-to-square"></i>`;
    editButton.addEventListener("click", () => this.editFlashcard(index));

    const deleteButton = document.createElement("button");
    deleteButton.classList.add("delete");
    deleteButton.innerHTML = `<i class="fa-solid fa-trash-can"></i>`;
    deleteButton.addEventListener("click", () => this.deleteFlashcard(index));

    const buttonsCon = document.createElement("div");
    buttonsCon.classList.add("buttons-con");
    buttonsCon.appendChild(editButton);
    buttonsCon.appendChild(deleteButton);

    cardDiv.appendChild(questionPara);
    cardDiv.appendChild(showHideButton);
    cardDiv.appendChild(answerPara);
   if( imageElement.src!="") cardDiv.appendChild(imageElement);
    cardDiv.appendChild(buttonsCon);

    return cardDiv;
};

HomePage.prototype.editFlashcard = function(index) {
    this.editedCardIndex = index;
    this.isBeingEdited = true;
    const flashcard = this.flashcards[index];
    this.question.value = flashcard.question;
    this.answer.value = flashcard.answer; 
    if (flashcard.image) {
        this.preview.src = flashcard.image;
        this.preview.classList.remove('hide');
    } else {
        this.preview.src = "";
        this.preview.classList.add('hide');
    }
    this.imageInput.nextElementSibling.textContent = flashcard.image ? "Image chosen" : "None chosen";
 
    this.disableButtons(true);
    this.addQuestionCard.classList.remove("hide");
    this.question.setAttribute("required", true);
    this.answer.setAttribute("required", true);
    this.question.focus();
};

HomePage.prototype.deleteFlashcard = function(index) {
    const deleteSound = document.getElementById("deleteSound");
    deleteSound.play();
    this.flashcards.splice(index, 1);
    localStorage.setItem("flashcards", JSON.stringify(this.flashcards));
    this.renderFlashcards();
};

HomePage.prototype.disableButtons = function(value) {
    const editButtons = document.getElementsByClassName("edit");
    Array.from(editButtons).forEach((element) => {
        element.disabled = value;
    });
};

// intro animation class
function GSAPAnimation() {
    if (localStorage.getItem('introPlayed')) {
        this.revealHomePage(true);
    } else {
        //hiding elements
        this.timeline = gsap.timeline({ defaults: { ease: 'power4.out', duration: 0.5 } });
        gsap.set('#root,#header,.footer', { opacity: 0, height: 0 });
        gsap.set('g', { autoAlpha: 1 });
        gsap.set("#flsh", { xPercent: -50 });

        gsap.to('svg', { scale: 1.2, duration: 2 });
        document.body.classList.add('overflow-hidden');

        this.createAnimation();
    }
}

GSAPAnimation.prototype.createAnimation = function() {
   
    this.timeline.from("#flsh", {
        yPercent: -200,
        stagger: 0.03,
        skewY: 60,
        skewX: 30,
        scaleY: 0.9,
        opacity: 0
    });

    this.timeline.from("#time", {
        yPercent: 215,
        stagger: 0.03,
        skewY: 60,
        skewX: 30,
        scaleY: 0.5,
        opacity: 0
    }, "-=.7");

    this.timeline.to("#flsh", {
        xPercent: 0,
        ease: 'elastic.out(1,.5)'
    }, "1.3");

    this.timeline.from("#arrow .st1", {
        xPercent: -507,
        opacity: 0,
        ease: 'elastic.out(1,.5)'
    }, "-=.7");

    this.timeline.to("#flsh, #time", {
        stagger: 0.03,
        scaleX: 0,
        x: 30,
        skewX: 30,
        duration: 0.2,
        scaleY: 0.5,
        opacity: 0
    }, "+=1");

    this.timeline.to("#arrow", {
        xPercent: 200,
        ease: 'elastic.out(1,.3)',
        duration: 1.3
    }, "<");

    this.timeline.to("#arrow", {
        rotateZ: -90,
        transformOrigin: 'center'
    }, "-=.1");

    this.timeline.to("#arrow", {
        yPercent: 1500,
        duration: 1.3
    }, "-=.5");

    this.timeline.to("#arrow", {
        yPercent: -2500,
        ease: 'back.in(1)',
        duration: 0.5
    }, "-=.3");

    this.timeline.to(".reveal", {
        scaleY: 0,
        transformOrigin: 'top',
        ease: 'power4.inOut',
        duration: 0.7,
        onComplete: () => {
            document.querySelector(".reveal").style.display = 'none';
            this.revealHomePage();
        }
    }, "-=.6");

};

GSAPAnimation.prototype.revealHomePage = function(skipAnimation = false) {
    //revealing elements of home page
    if (!skipAnimation) {
        gsap.to('#root,#header,.footer', { opacity: 1, height: 'auto', duration: 0.5 });
        gsap.to('.intro-container', { display: 'none', duration: 0 });
        localStorage.setItem('introPlayed', 'true');
    } else {
        document.querySelector('.intro-container').style.display = 'none';
        document.querySelector('#root').style.opacity = '1';
        document.querySelector('#root').style.height = 'auto';
    }
    document.body.classList.remove('overflow-hidden');

    this.reattachNavListeners();
    this.initHomePage();
};

GSAPAnimation.prototype.reattachNavListeners = function() {
    document.querySelectorAll('a[data-route]').forEach(anchor => {
        anchor.addEventListener('click', (event) => router.route(event));
    });
};

GSAPAnimation.prototype.initHomePage = function() {
    const homePage = new HomePage();
};

document.addEventListener("DOMContentLoaded", () => {
    if (window.location.hash === "" || window.location.hash === "#home") {
        new GSAPAnimation();
    } else {
        router.handleLocation();
    }
});